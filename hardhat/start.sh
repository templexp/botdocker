./build.sh
export DOCKER_NAME=test_hardhat
docker rm -f $DOCKER_NAME
sudo docker run --ulimit nofile=90000:90000 -p 11280:80 -p 13200:3000 -d -it \
-v /home/.logs:/root/.logs -v /wd0/projects/nodejs:/root/projects/ \
--name $DOCKER_NAME canadianbitcoin/hardhat /root/start.sh
docker exec -it $DOCKER_NAME  bash




