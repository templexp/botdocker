#./build.sh
export DOCKER_NAME=geth-node
docker rm -f $DOCKER_NAME
sudo docker run --ulimit nofile=90000:90000 -d -it -v /home/.logs:/root/.logs \
-v /wd0/.geth:/root/.ethereum \
--name $DOCKER_NAME canadianbitcoin/geth-node /root/start.sh


docker exec -it $DOCKER_NAME  sh
