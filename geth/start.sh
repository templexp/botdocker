#./build.sh
export DOCKER_NAME=geth_builder
docker rm -f $DOCKER_NAME
sudo docker run --ulimit nofile=90000:90000 -d -it -v /home/.logs:/root/.logs \
-v /root/.geth/bin:/go-ethereum/build/bin \
--name $DOCKER_NAME canadianbitcoin/geth /root/start.sh

docker exec -it $DOCKER_NAME  bash
