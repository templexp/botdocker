./build.sh
export DOCKER_NAME=dev_go
docker rm -f $DOCKER_NAME

mkdir /home/golang_$DOCKER_NAME/
sudo docker run --ulimit nofile=90000:90000 -d -it \
    -p 40000-49999:40000-49999 \
    -v /home/.logs:/root/.logs \
    -v /home/temple/.ssh:/root/.ssh/ \
    -v /home/golang_$DOCKER_NAME/:/root/go/src/ \
    --name $DOCKER_NAME canadianbitcoin/golang /root/start.sh
sudo docker exec -it $DOCKER_NAME  bash



