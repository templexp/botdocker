./build.sh
export DOCKER_NAME=debug_go
docker rm -f $DOCKER_NAME
sudo docker run --ulimit nofile=90000:90000 -d -it \
    -v /home/.logs:/root/.logs \
    -v /home/temple/.ssh:/root/.ssh/ \
    -v /home/golang_$DOCKER_NAME/:/root/projects/ \
    --name $DOCKER_NAME canadianbitcoin/golang /root/start.sh
docker exec -it $DOCKER_NAME  bash



