#./build.sh
export DOCKER_NAME=test_gotiny

docker rm -f $DOCKER_NAME
sudo docker run --ulimit nofile=90000:90000 -d -it -v /home/.logs:/root/.logs \
-v ~/projects:/root/projects \
-p 17700:17800 \
--name $DOCKER_NAME canadianbitcoin/gotiny /root/start.sh

docker exec -it $DOCKER_NAME  bash
