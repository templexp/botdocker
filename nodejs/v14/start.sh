./build.sh
export DOCKER_NAME=test_nodejs14
docker rm -f $DOCKER_NAME
sudo docker run --ulimit nofile=90000:90000 -p 11181-11200:11181-11200 -p 11180:80 -p 13000:3000 -d -it \
-v /home/.logs:/root/.logs -v /wd0/projects/nodejs:/root/projects/ \
--name $DOCKER_NAME canadianbitcoin/nodejs:v14 /root/start.sh
docker exec -it $DOCKER_NAME  bash



