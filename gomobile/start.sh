#./build.sh
export DOCKER_NAME=gomobile-test
docker rm -f $DOCKER_NAME
sudo docker run --ulimit nofile=90000:90000 -d -it -v /home/.logs:/root/.logs \
-v /wd0/.gomobile:/root/.gomobile \
-p 28555:28555 \
--name $DOCKER_NAME canadianbitcoin/gomobile bash

docker exec -it $DOCKER_NAME  bash
