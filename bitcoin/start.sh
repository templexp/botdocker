#./build.sh
export DOCKER_NAME=bitcoin_builder
export BTC_BIN=/wd0/projects/botdocker/bitcoin/node/bin
docker rm -f $DOCKER_NAME
sudo docker run --ulimit nofile=90000:90000 -d -it -v $BTC_BIN:/root/.bitcoin -v /home/.logs:/root/.logs -v /home/root/.bitcoin:/usr/local/bin --name $DOCKER_NAME canadianbitcoin/bitcoin /root/start.sh
docker exec -it $DOCKER_NAME  bash
