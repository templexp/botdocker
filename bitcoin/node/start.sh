#./build.sh
export DOCKER_NAME=bitcoin_node
export BTC_NODE=/wd0/.bitcoin
docker rm -f $DOCKER_NAME
sudo docker run --ulimit nofile=90000:90000 -p 8333:8333 -p 8332:8332 -d -it -v $BTC_NODE:/root/.bitcoin -v /home/.logs:/root/.logs -v /home/root/.bitcoin:/usr/local/bin --name $DOCKER_NAME canadianbitcoin/bitcoin-node /root/start.sh
docker exec -it $DOCKER_NAME  bash

