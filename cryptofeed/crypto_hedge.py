from decimal import Decimal

import json
import sys
import redis

from cryptofeed import FeedHandler
from cryptofeed.defines import CANDLES, BID, ASK, BLOCKCHAIN, FUNDING, GEMINI, L1_BOOK,  L2_BOOK, L3_BOOK, LIQUIDATIONS, OPEN_INTEREST, PERPETUAL, TICKER, TRADES, INDEX
from cryptofeed.exchanges import (FTX, Binance, BinanceUS, BinanceFutures, Bitfinex, Bitflyer, AscendEX, Bitmex, Bitstamp, Bittrex, Coinbase, Gateio,
                                  HitBTC, Huobi, HuobiDM, HuobiSwap, Kraken, OKCoin, OKX, Poloniex, Bybit, KuCoin, Bequant, Upbit, Probit)
from cryptofeed.exchanges.bitdotcom import BitDotCom
from cryptofeed.exchanges.bitget import Bitget
from cryptofeed.exchanges.cryptodotcom import CryptoDotCom
from cryptofeed.exchanges.delta import Delta
from cryptofeed.exchanges.fmfw import FMFW
from cryptofeed.exchanges.independent_reserve import IndependentReserve
from cryptofeed.exchanges.kraken_futures import KrakenFutures
from cryptofeed.exchanges.blockchain import Blockchain
from cryptofeed.exchanges.bithumb import Bithumb
from cryptofeed.symbols import Symbol
from cryptofeed.exchanges.phemex import Phemex
from cryptofeed.exchanges.dydx import dYdX
from cryptofeed.exchanges.deribit import Deribit

import asyncio
import redis.asyncio as redis

async def oi(t, receipt_timestamp):
    print(t)

async def funding(t, receipt_timestamp):
    jsonStr = json.dumps(t.to_dict(numeric_type=float))
    key=t.exchange+"-fund-"+t.symbol
    print("key:"+key)
    await redis_conn.publish(key, jsonStr)
    print(jsonStr)

async def liquidations(t, receipt_timestamp):
    print(t)




async def ticker(t, receipt_timestamp):
    print(t)


async def trade(t, receipt_timestamp):
    print(t)

async def book(book, receipt_timestamp):
    #convert to JSON string
    jsonStr = json.dumps(book.to_dict(numeric_type=float))

    #r = redis.Redis(host='192.168.2.222', port=6379, db=0)
    # r.set('foo', 'bar')        
    # r.set('foo', jsonStr)

    key=book.exchange+"-"+book.symbol
    #print("key:"+key)
    # r.publish('my-first-channel', jsonStr)
    await redis_conn.publish(key, jsonStr)

    # #print json string
    # print(jsonStr)

#     print(f'Book received at {receipt_timestamp} for {book.exchange} - {book.symbol}, with {len(book.book)} entries. Top of book prices: {book.book.asks.index(0)[0]} - {book.book.bids.index(0)[0]}')

#    # print(f' {book.book.asks.index(0)[0]} - {book.book.bids.index(0)[0]}')

#     n=0
#     for price in book.book.bids:
#         print(f"BID-> Price: {price} Size: {book.book.bids[price]}")
#         n=n+1
#         if n>5:
#             break

#     n=0
#     for price in book.book.asks:
#         print(f"ASK-> Price: {price} Size: {book.book.asks[price]}")
#         n=n+1
#         if n>5:
#             break


#     if book.delta:
#         print(f"Delta from last book contains {len(book.delta[BID]) + len(book.delta[ASK])} entries.")
#     if book.sequence_number:
#         assert isinstance(book.sequence_number, int)


def main():
    global redis_conn

    f = FeedHandler()
    
    # f.add_feed(dYdX(symbols=['BTC-USD-PERP'], channels=[L2_BOOK], callbacks={L2_BOOK: book}))
    # f.add_feed(FTX(symbols=['BTC-USD-PERP'], channels=[L2_BOOK], callbacks={L2_BOOK: book}))
    
    # ee=sys.argv[1].split(',')
    ss=sys.argv[2].split(',')

    print(ss)

    redis_conn = redis.Redis(host='127.0.0.1', port=6379, db=0)
    

    if sys.argv[1]=="FTX":
        f.add_feed(FTX(symbols=ss, channels=[L2_BOOK], callbacks={L2_BOOK: book}))
    elif  sys.argv[1]=="DYDX":
      for x in ss:
        s = []
        #print(x)
        s.append(x)
        f.add_feed(dYdX(symbols=s, channels=[L2_BOOK], callbacks={L2_BOOK: book}, max_depth=20, depth_interval='100ms'))
    elif  sys.argv[1]=="BINANCE":
      for x in ss:
        s = []
        #print(x)
        s.append(x)
        f.add_feed(BinanceFutures(symbols=s, channels=[L2_BOOK], callbacks={L2_BOOK: book}, max_depth=20, depth_interval='100ms'))
    elif  sys.argv[1]=="BUSD":
        f.add_feed(Binance(symbols=ss, channels=[L2_BOOK], callbacks={L2_BOOK: book}))

    elif  sys.argv[1]=="OKX":
      for x in ss:
        s = []
        print("OKX:"+x)
        s.append(x)
        f.add_feed(OKX(checksum_validation=True, symbols=s, channels=[L2_BOOK], callbacks={L2_BOOK: book}, max_depth=30, depth_interval='100ms'))

    elif  sys.argv[1]=="BYBIT":
      for x in ss:
        s = []
        print("BYBIT:"+x)
        s.append(x)
        f.add_feed(Bybit(symbols=s, channels=[L2_BOOK], callbacks={L2_BOOK: book}, max_depth=30, depth_interval='100ms'))

    elif  sys.argv[1]=="KRAKEN":
      for x in ss:
        s = []
        print("KRAKEN:"+x)
        s.append(x)
        f.add_feed(KrakenFutures(symbols=s, channels=[L2_BOOK], callbacks={L2_BOOK: book}, max_depth=30, depth_interval='100ms'))

    elif  sys.argv[1]=="GATEIO":
      #print( Gateio.symbols() )
      for x in ss:
        s = []
        print("GATEIO:"+x)
        s.append(x)
        f.add_feed(Gateio(symbols=s, channels=[L2_BOOK], callbacks={L2_BOOK: book}, max_depth=30, depth_interval='100ms'))
    elif  sys.argv[1]=="BINANCE-FUND":
#       f.add_feed(BinanceFutures(symbols=ss, channels=[TRADES, OPEN_INTEREST, FUNDING, LIQUIDATIONS], callbacks={TRADES: trade, OPEN_INTEREST: oi, FUNDING: funding, LIQUIDATIONS: liquidations}))
        f.add_feed(BinanceFutures(symbols=ss, channels=[FUNDING], callbacks={FUNDING: funding}))


    f.run()


if __name__ == '__main__':
    main()
