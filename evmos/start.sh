#./build.sh
export DOCKER_NAME=evmos-node
docker rm -f $DOCKER_NAME
sudo docker run --ulimit nofile=90000:90000 -d -it -v /home/.logs:/root/.logs \
-v /wd0/.evmosd:/root/.evmosd \
-p 26656:26656 \
-p 8545:8545 \
--name $DOCKER_NAME canadianbitcoin/evmos /root/start.sh

docker exec -it $DOCKER_NAME  bash
