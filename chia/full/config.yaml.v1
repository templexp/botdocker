ALERTS_URL: https://download.chia.net/notify/mainnet_alert.txt
CHIA_ALERTS_PUBKEY: 89b7fd87cb56e926ecefb879a29aae308be01f31980569f6a75a69d2a9a69daefd71fb778d865f7c50d6c967e3025937
chia_ssl_ca:
  crt: config/ssl/ca/chia_ca.crt
  key: config/ssl/ca/chia_ca.key
daemon_port: 55400
daemon_ssl:
  private_crt: config/ssl/daemon/private_daemon.crt
  private_key: config/ssl/daemon/private_daemon.key
farmer:
  full_node_peer:
    host: localhost
    port: 8444
  harvester_peer:
    host: localhost
    port: 8448
  logging: &id001
    log_filename: log/debug.log
    log_level: INFO
    log_stdout: false
  network_overrides: &id002
    config:
      mainnet:
        address_prefix: xch
      testnet0:
        address_prefix: txch
    constants:
      mainnet:
        GENESIS_CHALLENGE: ccd5bb71183532bff220ba46c268991a3ff07eb358e8255a65c30a2dce0e5fbb
        GENESIS_PRE_FARM_FARMER_PUZZLE_HASH: 3d8765d3a597ec1d99663f6c9816d915b9f68613ac94009884c4addaefcce6af
        GENESIS_PRE_FARM_POOL_PUZZLE_HASH: d23da14695a188ae5708dd152263c4db883eb27edeb936178d4d988b8f3ce5fc
        NETWORK_TYPE: 0
      testnet0:
        GENESIS_CHALLENGE: e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
        GENESIS_PRE_FARM_FARMER_PUZZLE_HASH: 3d8765d3a597ec1d99663f6c9816d915b9f68613ac94009884c4addaefcce6af
        GENESIS_PRE_FARM_POOL_PUZZLE_HASH: d23da14695a188ae5708dd152263c4db883eb27edeb936178d4d988b8f3ce5fc
        MIN_PLOT_SIZE: 18
        NETWORK_TYPE: 1
  pool_public_keys: !!set
    801c770b14ae54e7a56c156317c8596e2e02e326cae2e562d7e42888697a7e3cdbb4605d6f48a4bb256310d222587039: null
    a3dfd7a91c568613556e110d155369f0e70a91c4e4a5b22908e8a8a1c338a793f7d09f4418c478e12e738c62cda8859d: null
  pool_share_threshold: 1000
  port: 8447
  rpc_port: 8559
  selected_network: mainnet
  ssl:
    private_crt: config/ssl/farmer/private_farmer.crt
    private_key: config/ssl/farmer/private_farmer.key
    public_crt: config/ssl/farmer/public_farmer.crt
    public_key: config/ssl/farmer/public_farmer.key
  start_rpc_server: true
  xch_target_address: xch1nexak59evftrccf374a5juwty3aa5p0pzzu92cn08q3s0udlekas5yvnhg
full_node:
  database_path: db/blockchain_v1_CHALLENGE.sqlite
  enable_upnp: true
  farmer_peer:
    host: localhost
    port: 8447
  introducer_peer:
    host: introducer.chia.net
    port: 8444
  logging: *id001
  max_inbound_farmer: 10
  max_inbound_timelord: 5
  max_inbound_wallet: 20
  network_overrides: *id002
  peer_connect_interval: 30
  peer_db_path: db/peer_table_node.sqlite
  port: 8444
  recent_peer_threshold: 6000
  rpc_port: 8555
  selected_network: mainnet
  send_uncompact_interval: 0
  short_sync_blocks_behind_threshold: 20
  simulator_database_path: sim_db/simulator_blockchain_v1_CHALLENGE.sqlite
  simulator_peer_db_path: sim_db/peer_table_node.sqlite
  ssl:
    private_crt: config/ssl/full_node/private_full_node.crt
    private_key: config/ssl/full_node/private_full_node.key
    public_crt: config/ssl/full_node/public_full_node.crt
    public_key: config/ssl/full_node/public_full_node.key
  start_rpc_server: true
  sync_blocks_behind_threshold: 300
  target_outbound_peer_count: 10
  target_peer_count: 60
  target_uncompact_proofs: 100
  timelord_peer:
    host: localhost
    port: 8446
  wallet_peer:
    host: localhost
    port: 8449
  weight_proof_timeout: 180
harvester:
  chia_ssl_ca:
    crt: config/ssl/ca/chia_ca.crt
    key: config/ssl/ca/chia_ca.key
  farmer_peer:
    host: localhost
    port: 8447
  logging: *id001
  network_overrides: *id002
  num_threads: 30
  plot_directories:
  - /root/projects/chia-blockchain
  - /root/projects/chia-blockchain/plot
  - /data/plot
  - /data/plottemp/plot
  - /mnt/sdd1/plot
  - /wd
  - /wd/fist
  - /wd/plot
  - /plot5
  - /wd/chiaplot
  - /wd5
  - /wd6
  - /wd7
  - /wd4
  - /wd2
  - /hpi7
  port: 8448
  private_ssl_ca:
    crt: config/ssl/ca/private_ca.crt
    key: config/ssl/ca/private_ca.key
  rpc_port: 8560
  selected_network: mainnet
  ssl:
    private_crt: config/ssl/harvester/private_harvester.crt
    private_key: config/ssl/harvester/private_harvester.key
  start_rpc_server: true
inbound_rate_limit_percent: 100
introducer:
  host: localhost
  logging: *id001
  max_peers_to_send: 20
  network_overrides: *id002
  port: 8445
  recent_peer_threshold: 6000
  selected_network: mainnet
  ssl:
    public_crt: config/ssl/full_node/public_full_node.crt
    public_key: config/ssl/full_node/public_full_node.key
logging: *id001
min_mainnet_k_size: 32
network_overrides: *id002
outbound_rate_limit_percent: 30
ping_interval: 120
pool:
  logging: *id001
  network_overrides: *id002
  selected_network: mainnet
  xch_target_address: xch1nexak59evftrccf374a5juwty3aa5p0pzzu92cn08q3s0udlekas5yvnhg
private_ssl_ca:
  crt: config/ssl/ca/private_ca.crt
  key: config/ssl/ca/private_ca.key
selected_network: mainnet
self_hostname: localhost
timelord:
  fast_algorithm: false
  full_node_peer:
    host: localhost
    port: 8444
  logging: *id001
  max_connection_time: 60
  network_overrides: *id002
  port: 8446
  sanitizer_mode: false
  selected_network: mainnet
  ssl:
    private_crt: config/ssl/timelord/private_timelord.crt
    private_key: config/ssl/timelord/private_timelord.key
    public_crt: config/ssl/timelord/public_timelord.crt
    public_key: config/ssl/timelord/public_timelord.key
  vdf_clients:
    ip:
    - localhost
    - localhost
    - 127.0.0.1
    ips_estimate:
    - 150000
  vdf_server:
    host: localhost
    port: 8000
timelord_launcher:
  logging: *id001
  port: 8000
  process_count: 3
ui:
  daemon_host: localhost
  daemon_port: 55400
  daemon_ssl:
    private_crt: config/ssl/daemon/private_daemon.crt
    private_key: config/ssl/daemon/private_daemon.key
  logging: *id001
  network_overrides: *id002
  port: 8222
  rpc_port: 8555
  selected_network: mainnet
  ssh_filename: config/ssh_host_key
wallet:
  database_path: wallet/db/blockchain_wallet_v1_CHALLENGE_KEY.sqlite
  full_node_peer:
    host: localhost
    port: 8444
  initial_num_public_keys: 100
  initial_num_public_keys_new_wallet: 5
  introducer_peer:
    host: introducer.chia.net
    port: 8444
  logging: *id001
  network_overrides: *id002
  num_sync_batches: 50
  peer_connect_interval: 60
  port: 8449
  recent_peer_threshold: 6000
  rpc_port: 9256
  selected_network: mainnet
  ssl:
    private_crt: config/ssl/wallet/private_wallet.crt
    private_key: config/ssl/wallet/private_wallet.key
    public_crt: config/ssl/wallet/public_wallet.crt
    public_key: config/ssl/wallet/public_wallet.key
  start_height_buffer: 100
  starting_height: 0
  target_peer_count: 5
  testing: false
  trusted_peers:
    public_crt: config/ssl/full_node/public_full_node.crt
  wallet_peers_path: wallet/db/wallet_peers.sqlite
