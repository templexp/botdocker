#./build.sh
docker rm -f test_chiav6
docker run --env DOCKERHOST=$HOSTNAME --privileged --restart=always --ulimit nofile=90000:90000 -d -it -p 8080:8080 -p 18080:18080 -v /home/.logs:/root/.logs --name test_chiav6 canadianbitcoin/chiav6 /root/start.sh
docker exec -it test_chiav6 bash

