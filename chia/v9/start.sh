#./build.sh
ID=v8
HOSTNAME=192.168.2.132

docker rm -f chia$ID 
docker run --env DOCKERHOST=$HOSTNAME --privileged --restart=always --ulimit nofile=90000:90000 -d -it -p 8080:8080 -p 18080:18080 -v /home/.logs:/root/.logs --name test_chia$ID canadianbitcoin/chia$ID /root/start.sh
docker exec -it test_chia$ID bash
